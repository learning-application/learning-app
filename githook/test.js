const { exec } = require('child_process')

function execute() {
  let command = `cd ../server && git pull && docker restart server`
  return new Promise((resolve, reject) => {
      exec(command, (err, stdout, stderr) => {
          if (err) {
              reject(err)
          }
          resolve(stdout ? stdout : stderr)
      })
  })
}
execute()