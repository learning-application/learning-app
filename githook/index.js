const port = 5000

const express = require('express'),
  yaml = require('yaml'),
  fs = require('fs'),
  bodyParser = require('body-parser'),
  { exec } = require('child_process')

const public_ip = process.env.PUBLIC_IP

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


const process_hook_dev = async (req, res) => {
  console.log("Connect Githook");
  
  res.status(200).json({ success: true })
  let command = `ssh -i ./keys/key hungnv_ct@35.247.128.165 eval `
  if (req.body.repository.name !== 'server') {
    command = command + `'cd ~/learning-app/${req.body.repository.name} && git pull && docker restart ${req.body.repository.name}'`
  } else {
    command = command + `'cd ~/learning-app/${req.body.repository.name} && git pull'`
  }
   
  function execute() {
    return new Promise((resolve, reject) => {
        exec(command, (err, stdout, stderr) => {
            if (err) {
              reject(err)
            }
            resolve(stdout ? stdout : stderr)
        })
    })
  }
  if (req.body.push.changes[0].new.name === 'prod') {
    execute()
      .then((res) => console.log(res, "Githook Success", req.body.repository.name))
      .catch((err) => console.log(err, "Error"))
  } else {
    console.log("Error ============================= Githook")
  }
}
app.get('/dev', process_hook_dev)
app.post('/dev', process_hook_dev)
app.get('/test',(req, res) => {
  res.json({ success: true })
})


app.listen(port, () =>
  console.log(`Githook server listening on port ${port} at ${public_ip}`)
)
